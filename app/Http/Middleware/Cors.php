<?php
/**
 * Created by PhpStorm.
 * User: Patrik
 * Date: 13.04.2019
 * Time: 10:54
 */

namespace App\Http\Middleware;

use Closure;


class Cors
{
    public function handle($request, Closure $next) {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, Content-Type, Authorization');
    }

}