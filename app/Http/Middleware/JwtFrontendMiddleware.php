<?php

namespace App\Http\Middleware;

 use Closure;
 use Config as config;
 use JWTAuth;
 use Exception;
 use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

 class JwtFrontendMiddleware extends BaseMiddleware
 {

     /**
      * Handle an incoming request.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  \Closure  $next
      * @return mixed
      */
     public function handle($request, Closure $next)
     {
         try {
             $user = JWTAuth::parseToken()->authenticate();
         } catch (Exception $e) {
             if (config('vars.debug_mode')){
               return redirect('http://localhost:4200/login');
             }
             else {
               return redirect()->route('login');
             }
         }

         return $next($request);

     }
 }
