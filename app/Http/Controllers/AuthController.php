<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;
use App\Providers\AuthService;
use Illuminate\Support\Facades\App;

class AuthController extends Controller {
    protected $m_authService;

    public function __construct(AuthService $p_authService) {
        $this->m_authService = new AuthService();
    }

    public function login(Request $p_request) {
        $l_response = null;

        try {
            $l_token = $this->m_authService->login($p_request);

            $l_response = response($l_token, 200)
                            ->header('Content-Type', 'application/json');
        }
        catch (\App\Exceptions\AuthenticationFaultException $l_ex) {
            $l_response = response('Authentication failed', 401)
                            ->header('Content-Type', 'application/json');
        }

        return $l_response;
    }

    public function register(Request $p_request) {
        $l_response = null;

        try {
            $l_user = $this->m_authService->registerUser($p_request);

            $l_response = response($l_user, 200)
                            ->header('Content-Type', 'application/json');
        }
        catch (\App\Exceptions\UserDuplicateEntryException $l_ex) {
           $l_response = response('Duplicate entry', 409)
                    ->header('Content-Type', 'application/json');
        }

        return $l_response;

        
    }
}
