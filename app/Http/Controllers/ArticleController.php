<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;
use App\Providers\ArticleService;
use App\Entities\Article;
use Illuminate\Support\Facades\App;
use App\Exceptions\ForeignKeyFaultException;
use App\Exceptions\MissingMandatoryFieldException;

class ArticleController extends Controller
{
    protected $m_articleService;

    public function __construct(ArticleService $p_articleService) {
        $this->m_articleService = new ArticleService();
    }

    public function addArticle(Request $p_request) {
        $l_response = null;
        $l_article = Article::mapJsonToObject($p_request->getContent());
        $l_article->id = 0;
        
        
    
        try {
            $this->m_articleService->addArticle($l_article);
        }
        catch (\App\Exceptions\ForeignKeyFaultException $p_ex) {
           $l_response = response('Foreign key fault', 404)
            ->header('Content-Type', 'application/json');
        }
        catch (\App\Exceptions\MissingMandatoryFieldException $p_ex) {
            $l_response = response('Missing mandatory field', 404)
            ->header('Content-Type', 'application/json');
        }

        return $l_response;
        
    }

    public function editArticle(Request $p_request){
        $l_response = null;
        $l_article = Article::mapJsonToObject($p_request->getContent());

        $l_article = Article::mapJsonToObject($p_request->getContent());
        $l_article->id = 2;
        $l_article->user_id = 1;
        $l_article->category_id = 1;
        $l_article->cover = "image";

        try {
            $this->m_articleService->editArticle($l_article);
        }
        catch (\App\Exceptions\ForeignKeyFaultException $p_ex) {
           $l_response = response('Foreign key fault', 404)
            ->header('Content-Type', 'application/json');
        }

        return $l_response;
    }
 
}
