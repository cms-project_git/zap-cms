<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Providers\CategoryService;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    private $categoryService;

    public function __construct() {
      $this->categoryService = new CategoryService();
      auth()->setDefaultDriver('api');
    }
}
