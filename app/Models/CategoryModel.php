<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class CategoryModel
{
    protected $table = 'category';

    protected $fillable = ['name', 'parent_id'];
}
