<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserModel extends Authenticatable implements JWTSubject
{
    protected $table = 'user';

    protected $hidden = ['password'];

    protected $fillable = ['nickname', 'password', 'name', 'surname', 'email'];

    public function getJWTIdentifier() {
      return $this->getKey();
    }

    public function getJWTCustomClaims() {
      return [];
    }

    public function setPasswordAttribute($p_password) {
      if (!empty($p_password)) {
        $this->attributes['password'] = bcrypt($p_password);
      }
    }

}
