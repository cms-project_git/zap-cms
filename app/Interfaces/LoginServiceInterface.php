<?php
/**
 * Created by PhpStorm.
 * User: Patrik
 * Date: 13.04.2019
 * Time: 12:15
 */

namespace App\Interfaces;


interface LoginServiceInterface
{
    public function login($l_body);
}