<?php
namespace App\Entities;

class Category {
  public $name;

  public $parent_id;

  public static function mapJsonToObject($p_json) {
    $l_className = __CLASS__;
    $l_object = new $l_className();

    $props = get_object_vars($l_object);
    $jsonDecoded = json_decode($p_json);
    $jsonToArr = (array) $jsonDecoded;
    
    foreach ($props as $key => $value) {
        if (isset($jsonToArr[$key])) {
            $l_object->$key = $jsonToArr[$key];
        }
        else {
            $l_object->$key = null;
        }
        
    }
    
    return $l_object;
  }

  public function mapObjectToJson() {
    return get_object_vars($this);
  }
}
