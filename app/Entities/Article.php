<?php
namespace App\Entities;

class Article {
  public $id;
  /*
    @PropertyTypeAnnotation(type="string")
  */ 
  public $title;

  /*
    @PropertyTypeAnnotation(type="string")
  */ 
  public $perex;

  /*
    @PropertyTypeAnnotation(type="string")
  */ 
  public $content;

  /*
    @PropertyTypeAnnotation(type="string")
  */ 
  public $slug;

  /*
    @PropertyTypeAnnotation(type="string")
  */ 

  public $user_id;

  public $category_id;

  public $cover;

  public static function mapJsonToObject($p_json) {
    $l_className = __CLASS__;
    $l_object = new $l_className();

    $props = get_object_vars($l_object);
    $jsonDecoded = json_decode($p_json);
    $jsonToArr = (array) $jsonDecoded;
    
    foreach ($props as $key => $value) {
        if (isset($jsonToArr[$key])) {
            $l_object->$key = $jsonToArr[$key];
        }
        else {
            $l_object->$key = null;
        }
        
    }
    
    return $l_object;
  }

  public function mapObjectToJson() {
    return get_object_vars($this);
  }
}
