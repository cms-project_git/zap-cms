<?php 

namespace App\Entities;

class ErrorDTO {
    private $objects;
    
    private $message;

    public function __construct($p_column, $p_objects) {
        $this->objects = $p_objects;
        $this->message = $p_message;
    }

    public function mapObjectToJson() {
        return get_object_vars($this);
    }
}