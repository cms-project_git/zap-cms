<?php 

namespace App\Entities;

class BaseEntity {
    public static function mapJsonToObject($p_json) {
        $l_className = __CLASS__;
        $l_object = new $l_className();
    
        $props = get_object_vars($l_object);
        $jsonDecoded = json_decode($p_json);
        $jsonToArr = (array) $jsonDecoded;
        
        foreach ($props as $key => $value) {
          $l_object->$key = $jsonToArr[$key];
        }
        
        dd($l_object);
        return $l_object;
      }
}