<?php

namespace App\Exceptions;

use Exception;

class AuthenticationFaultException extends Exception
{
    private $errorObject;

    public function __construct($p_errorObject) {
        $this->errorObject = $p_errorObject;
    }

    public function getErrorObject() {
        return $this->errorObject;
    }

    public function report(){}

    public function render(){}


}
