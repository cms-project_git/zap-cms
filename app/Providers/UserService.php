<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use App\Models\UserModel;
use JWTAuth;

class UserService extends ServiceProvider
{
    private $m_userModel;

    public function __construct(){
        $this->m_userModel = new UserModel();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
