<?php 

namespace App\Providers;

use App\Models\ArticleModel;
use App\Entities\Article;
use App\Exceptions\ForeignKeyFaultException;
use App\Exceptions\MissingMandatoryFieldException;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ArticleService {
    
    private $m_articleModel;

    const FOREIGN_KEY_FAULT = 1452;
    const MISSING_MANDATORY_FIELD = 1048;

    public function __construct() {}
    
    public function addArticle(Article $p_article) {
        $p_article->slug = Str::slug($p_article->title);
        /* Id must be contained but will be reoplaced */
        $p_article->id = 0;

        try {
            $l_article = ArticleModel::create($p_article->mapObjectToJson());
        }
        catch (\Illuminate\Database\QueryException $p_ex) {
            $l_errorCode = $p_ex->errorInfo[1];
            $l_errorMessage = $p_ex->errorInfo[2];

            if ($l_errorCode === $this::FOREIGN_KEY_FAULT) {
                throw new ForeignKeyFaultException();
            }
            else if ($l_errorCode === $this::MISSING_MANDATORY_FIELD) {
                throw new MissingMandatoryFieldException("");
            }
        }
        
    }

    public function editArticle(Article $p_article){
        $p_article->slug = Str::slug($p_article->title);

        try {
            DB::table('article')->where('id', $p_article->id)
                                ->update($p_article->mapObjectToJson());
        }
        catch (\Illuminate\Database\QueryException $p_ex) {
            $l_errorCode = $p_ex->errorInfo[1];
            $l_errorMessage = $p_ex->errorInfo[2];

            if ($l_errorCode === $this::FOREIGN_KEY_FAULT) {
                throw new ForeignKeyFaultException();
            }
            else if ($l_errorCode === $this::MISSING_MANDATORY_FIELD) {
                throw new MissingMandatoryFieldException("");
            }
        }
    }
}