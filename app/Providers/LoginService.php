<?php

namespace App\Providers;

use App\Interfaces\LoginServiceInterface;
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use App\Models\UserModel;
use JWTAuth;

class LoginService extends ServiceProvider
{
    private $m_userModel;

    public function __construct(){
        $this->m_userModel = new UserModel();
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register(){

    }


    public function login(Request $p_request) {
        $l_credentials = $p_request->only(['email', 'password']);

        if (!$l_token = JWTAuth::attempt($l_credentials)){
          return response()->json(['error' => 'Uživatel nebyl autorizován.'], 401);
        }
        return response()->json(['token' => $l_token]);
    }


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
