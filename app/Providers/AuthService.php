<?php

namespace App\Providers;

use App\Exceptions\UserDuplicateEntryException;
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Application;
use App\Entities\User;
use Illuminate\Http\Request;
use App\Models\UserModel;
use JWTAuth;

class AuthService extends ServiceProvider
{
    private $m_userModel;

    const DUPLICATE_ENTRY_CODE = 1062;

    public function __construct(){
        $this->m_userModel = new UserModel();
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function registerUser(Request $p_request){
           
        // $l_token = auth()->login($l_user);
        try {
            $l_user = User::mapJsonToObject($p_request->getContent());
            $l_userModel = UserModel::create($l_user->mapObjectToJson());
            
            return $l_userModel;
        }
        catch (\Illuminate\Database\QueryException $p_ex) {
            $l_errorCode = $p_ex->errorInfo[1];
            $l_errorMessage = $p_ex->errorInfo[2];

            if ($l_errorCode === $this::DUPLICATE_ENTRY_CODE) {
                throw new UserDuplicateEntryException('message');
            }
        }
        

        

        // return $this->respondWithToken($l_token);
    }


    public function login(Request $p_request) {
        $l_credentials = $p_request->only(['nickname', 'password']);


        if (!$l_token = auth()->attempt($l_credentials)){
          throw new \App\Exceptions\AuthenticationFaultException("Bad login");
        }


        return $this->wrapToken($l_token);
    }

    protected function wrapToken($p_token)
    {
        $l_wrappedToken = [
            'access_token' => $p_token,
            'token_type' => 'Bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60
        ];
        
        return $l_wrappedToken;
    }


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
