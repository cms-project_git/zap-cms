import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

/* Services */
import { AuthService } from 'src/app/service/auth/auth.service';

/* environment for variables */
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient, private authService: AuthService) { }

  doLogin(nickname, password):Promise<any>{

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
      responseType: 'text' as 'json'
    }

    var credentials: any = {};
    credentials.nickname = nickname;
    credentials.password = password;

    return this.httpClient
         .post(environment.apiEndpoint + "/login", credentials, httpOptions)
         .toPromise()
         .then((data) => {
             this.setLoggedIn();
             this.authService.setToken(data);
             return data;
         })
         .catch((err) => {
            return Promise.reject(err);
         });
  }

  setLoggedIn(): void {
    localStorage.setItem("loggedIn", "true");
  }

  isLoggedIn(): boolean {
    if (localStorage.getItem("loggedIn") == "true") {
      return true;
    }
    else return false;

  }
}
