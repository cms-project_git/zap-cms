import { TestBed } from '@angular/core/testing';

import { MediapoolService } from './mediapool.service';

describe('MediapoolService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MediapoolService = TestBed.get(MediapoolService);
    expect(service).toBeTruthy();
  });
});
