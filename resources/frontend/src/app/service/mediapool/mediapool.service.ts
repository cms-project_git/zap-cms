import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MediapoolService {

  constructor(private httpClient: HttpClient) { }

  getAllImages():Promise <any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
      responseType: 'text' as 'json'
    }

    return this.httpClient.get(environment.apiEndpoint + "/mediapool/getAllMediapools", httpOptions).
        toPromise()
        .then((data) => {
          return data;
        })
        .catch((err) => {
           return Promise.reject(err);
        });
  }

  makeImageUrl(name) {
    return environment.endpoint + "/mediapool/" + name;
  }
}
