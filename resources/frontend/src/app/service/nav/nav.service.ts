import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavService {
  public visible: boolean;

  constructor() { }

  hideNav(){
    this.visible = false;
  }

  showNav(){
    this.visible = true;
  }
}
