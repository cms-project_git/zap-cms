import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { DateParser } from 'src/app/class/date-parser';
import { ArticleExpanded } from 'src/app/class/entity/article-expanded';

/* environment for variables */
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private httpClient: HttpClient,
              private dateParser: DateParser) {}

  addArticle(values):Promise <any> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
      responseType: 'text' as 'json'
    }

    return this.httpClient.post(environment.apiSecureEndpoint + "/article/add", values, httpOptions)
        .toPromise()
        .then((data) => {
          return data;
        })
        .catch((err) => {
           return Promise.reject(err);
        });
  }

  getAllArticles():Promise <any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
      responseType: 'text' as 'json'
    }

    return this.httpClient.get(environment.apiEndpoint + "/article/getAll", httpOptions)
        .toPromise()
        .then(data => {
          // var parsed:any = JSON.parse(data);
          // var articlesOutput:ArticleExpanded[] = new Array();
          //
          // for (var i = 0; i < parsed.length; i++) {
          //   var article = new ArticleExpanded(parsed[i]);
          //   articlesOutput.push(article);
          // }
          // return articlesOutput;
        })
        .catch((err) => {
           return Promise.reject(err);
        });
  }

  parseForm(formGroup, image) {
    var json = formGroup.value;
    var date = this.dateParser.parseDate(json.date);

    json.image = image;
    json.date = date;

    return json;
  }
}
