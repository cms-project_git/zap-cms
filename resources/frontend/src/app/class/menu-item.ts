export class MenuItem {
    private url: String;
    private iconPath: String;
    private name: String;
    private nextItems: MenuItem[];

    constructor(url: String, iconPath: String, name: String, nextItems: MenuItem[]) {
        this.url = url;
        this.iconPath = iconPath;
        this.name = name;
        this.nextItems = nextItems;
    }

    getUrl(){
      return this.url;
    }

    setUrl(url: String) {
      this.url = url;
    }
    getIconPath(){
      return this.iconPath;
    }

    setIconPath(iconPath: String) {
      this.iconPath = iconPath;
    }

    getName(){
      return this.name;
    }

    setName(name: String) {
      this.name = name;
    }

    getNextItems() {
      return this.nextItems;
    }

    setNextItems(nextItems: MenuItem[]) {
      this.nextItems = nextItems;
    }

    // Other methods

    public containsHref() {
      return (this.getUrl() == null) ? false : true;
    }


}
