import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DateParser {
  parseDate(inputDate): string {
    var year = inputDate.year;
    var month = inputDate.month;
    var day = inputDate.day;

    if (month < 10) {
      month = "0" + month;
    }
    if (day < 10) {
      day = "0" + day;
    }

    return year + "-" + month + "-" + day;
  }
}
