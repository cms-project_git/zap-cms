import { Injectable } from '@angular/core';
import {CanActivate} from "@angular/router";

/* Service */
import {LoginService} from 'src/app/service/login/login.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedGuard implements CanActivate {

  constructor(private loginService: LoginService){}

  canActivate() {
    if (this.loginService.isLoggedIn()){
      return true;
    }
    return false;
  }
}
