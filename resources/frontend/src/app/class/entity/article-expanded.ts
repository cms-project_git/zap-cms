import { Article } from 'src/app/class/entity/article';
import { User } from 'src/app/class/entity/user';
import { Category } from 'src/app/class/entity/category';

export class ArticleExpanded {
  article: Article;
  user: User;
  category: Category;

  constructor(data) {
    this.article = new Article(data.id, data.title, data.content, data.perex, data.date, data.image);
    this.user = new User(data.author);
    this.category = new Category(data.category);
  }
}
