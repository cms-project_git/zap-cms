export class User {
  id;
  nickname;
  name;
  surname;
  avatar;
  password;

  constructor(data) {

    this.id = data.id;
    this.nickname = data.nickname;
    this.name = data.name;
    this.surname = data.surname;
    this.avatar = data.avatar;
    this.password = data.password;

  }
}
