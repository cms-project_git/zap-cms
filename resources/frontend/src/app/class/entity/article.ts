export class Article {
  id;
  title;
  content;
  perex;
  date;
  image;
  userId;
  categoryId;

  constructor(id, title, content, perex, date, image) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.perex = perex;
    this.date = date;
    this.image = image;

    console.log(this);
  }
}
