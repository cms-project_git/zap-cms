export class Category {
  id;
  name;
  parent;

  constructor(data) {
    this.id = data.id;
    this.name = data.name;
    this.parent = data.parent;
  }
}
