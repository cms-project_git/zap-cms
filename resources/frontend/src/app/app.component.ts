import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  showNav = true;

  constructor(private router: Router) {

  }

  ngOnInit() {
    //hide menu if login page
    this.router.events.subscribe(event => {
      if (event.constructor.name === "NavigationEnd") {
        var url = (<any>event).url.split("/").slice(-1)[0];
        if (url === "login") this.showNav = false;
        else this.showNav = true;
      }
    })
  }
}
