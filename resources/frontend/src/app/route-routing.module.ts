import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Component */
import {LoginComponent} from './component/login/login.component';
import {MainboardComponent} from './component/mainboard/mainboard.component';
import {UserOverviewComponent} from './component/user/user-overview/user-overview.component';
import {ArticleAddComponent} from './component/article/article-add/article-add.component';
import {ArticleOverviewComponent} from './component/article/article-overview/article-overview.component';
import {CategoryOverviewComponent} from './component/category/category-overview/category-overview.component';

/* Service */
import {LoggedGuard} from 'src/app/class/logged-guard/logged-guard';

// Routes list
const routes: Routes = [
  {
      path: 'login',
      component: LoginComponent
  },
  {
       path: 'app/mainboard',
       component: MainboardComponent,
  },
  {
       path: 'app/user/overview',
       component: UserOverviewComponent
  },
  {
       path: 'app/article/add',
       component: ArticleAddComponent
  },
  {
       path: 'app/article/overview',
       component: ArticleOverviewComponent
  },
  {
       path: 'app/category/overview'

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RouteRoutingModule { }
