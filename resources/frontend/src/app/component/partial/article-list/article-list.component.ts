import { Component, OnInit, Input } from '@angular/core';
import { ArticleService } from 'src/app/service/article/article.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {

  @Input() articles;

  constructor(private articleService : ArticleService,
              private toastrService: ToastrService) {

  }

  ngOnInit() {
  }


}
