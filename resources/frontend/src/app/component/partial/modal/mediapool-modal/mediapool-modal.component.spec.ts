import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediapoolModalComponent } from './mediapool-modal.component';

describe('MediapoolModalComponent', () => {
  let component: MediapoolModalComponent;
  let fixture: ComponentFixture<MediapoolModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediapoolModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediapoolModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
