import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MediapoolService } from 'src/app/service/mediapool/mediapool.service';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';

@Component({
  selector: 'app-mediapool-modal',
  templateUrl: './mediapool-modal.component.html',
  styleUrls: ['./mediapool-modal.component.css']
})
export class MediapoolModalComponent implements OnInit {

  @Output() onImagePicked: EventEmitter<any> = new EventEmitter<any>();
  pickedImage;
  lastElement = null;
  images;

  constructor(private modalService: NgbModal,
              private mediapoolService: MediapoolService,
              private toastrService: ToastrService) {}


  ngOnInit(){
    this.getAllImages();
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

  pickImage($event) {
    var clickedElement = $event.target.parentNode;
    var items = $('.choosen-item');

    this.pickedImage = clickedElement.dataset.filename;
    this.onImagePicked.emit(this.pickedImage);

    this.lastElement = clickedElement;

    if (items.length > 0) {
      $(items).remove();
    }

    $(clickedElement).append($('<i class="fas fa-2x fa-check-circle choosen-item"></i>'));

  }

  makeImageUrl(name) {
    return this.mediapoolService.makeImageUrl(name);
  }

  getAllImages(): any {
    this.mediapoolService.getAllImages()
        .then((data) =>{
          this.images = JSON.parse(data);
        })
        .catch((data) =>{
          console.log(data);
        })
  }

}
