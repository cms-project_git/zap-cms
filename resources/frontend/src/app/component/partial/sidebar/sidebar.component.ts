import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'src/app/class/menu-item';
import { NavService } from 'src/app/service/nav/nav.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  articleSubItems = [
      new MenuItem("article/add", "fas fa-plus", "Přidat", null),
      new MenuItem("article/overview", "fas fa-list", "Přehled", null)
  ]

  userSubItems = [
      new MenuItem("user/overview", "fas fa-list", "seznam uživatelů", null)
  ]

  menuItems = [
      new MenuItem(null, "fas fa-edit", "články", this.articleSubItems),
      new MenuItem(null, "fas fa-users", "uživatelé", this.userSubItems),
      new MenuItem("123", "fas fa-list-alt", "kategorie", null)
  ]

  initializationOfMenuLogic() {
    $(".expander").click(function (e){
        e.preventDefault();

        var parentElement = ($(this)).parent();
        var expandableMenu = parentElement.find('.menu-list');
        var icon = ($(this)).find('.menu-item-icon').first();
        var iconArrow = ($(this)).find('.arrow-icon').first();

        if (expandableMenu.is(":visible")) {
          expandableMenu.fadeOut();
          icon.removeClass("active");
          iconArrow.removeClass("fa-chevron-up");
          iconArrow.addClass("fa-chevron-down");
        }
        else {
          expandableMenu.fadeIn();
          icon.addClass("active");
          iconArrow.addClass("fa-chevron-up");
          iconArrow.removeClass("fa-chevron-down");
        }

    });
  }

  fixedMenuLogic() {
    $(document).scroll(function(e){

      console.log($(window).height());
      if ($(window).width() > 850){
        $('aside').addClass('fixed-wrapper');
        $('main').css('margin-left', $('aside').width());
      }
      else {
        $('aside').removeClass('fixed-wrapper');
        $('main').css('margin-left', 0);
      }


    })
  }

  constructor() {

  }

  ngOnInit(){

  }

  ngAfterViewInit(){
    this.initializationOfMenuLogic();
    this.fixedMenuLogic();
  }



}
