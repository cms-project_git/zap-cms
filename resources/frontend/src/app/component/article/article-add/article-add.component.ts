import { Component, OnInit, Input } from '@angular/core';
import { ArticleService } from 'src/app/service/article/article.service';
import { MediapoolService } from 'src/app/service/mediapool/mediapool.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MediapoolModalComponent } from 'src/app/component/partial/modal/mediapool-modal/mediapool-modal.component';
import { environment } from 'src/environments/environment'
import { CKEditorModule } from 'ngx-ckeditor';
import { ToastrService } from 'ngx-toastr';
import * as NgbModule from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-article-add',
  templateUrl: './article-add.component.html',
  styleUrls: ['./article-add.component.css']
})
export class ArticleAddComponent implements OnInit {

  articleForm;
  pickedImage = null;

  public configPerex = {
    'height': '200px'
  };
  public configContent = {
    'height': '500px'
  };

  constructor(private articleService: ArticleService,
              private toastrService: ToastrService,
              private mediapoolService: MediapoolService) {}

  ngOnInit() {
    this.articleForm = new FormGroup({
      title: new FormControl('', [
         Validators.required
      ]),
      categoryId: new FormControl('', [
         Validators.required
      ]),
      date: new FormControl('', [
         Validators.required
      ]),
      perex: new FormControl('', [
         Validators.required
      ]),
      content: new FormControl('', [
         Validators.required
      ])
    });
  }

  onImagePicked(filename) {
    this.pickedImage = filename;
  }

  makeImageUrl(name) {
    if (name) {
      return this.mediapoolService.makeImageUrl(name);
    }

  }

  addArticle() {
    console.log(this.pickedImage);
    if (this.articleForm.valid) {
      this.articleService.addArticle(this.articleService.parseForm(this.articleForm, this.pickedImage))
          .then((data) => {
            console.log(data);
          })
          .catch((data) => {
            console.log(data);
            this.toastrService.error('Zřejmě došlo k chybě při komunikaci se serverem, prosím opakujte akci později.', 'Článek nebyl přidán', {
              positionClass: 'toast-bottom-right'
            });
          })
    }
    else {
      this.toastrService.error('Pro přidání článku musíte vyplnit všechna povinná pole.', 'Vyplňte všechna povinná pole', {
        positionClass: 'toast-bottom-right'
      });
    }


  }

}
