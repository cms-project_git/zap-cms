import { Component, OnInit } from '@angular/core';
import { ArticleListComponent } from 'src/app/component/partial/article-list/article-list.component';
import { ArticleService } from 'src/app/service/article/article.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-article-overview',
  templateUrl: './article-overview.component.html',
  styleUrls: ['./article-overview.component.css']
})
export class ArticleOverviewComponent implements OnInit {

  articles = "";
  constructor(private articleService: ArticleService,
              private toastrService: ToastrService) { }

  ngOnInit() {
    // this.articleService.getAllArticles().then((data) => {
    //    this.articles = data;
    //    console.log(data);
    // })
    // .catch((err) => {
    //   this.toastrService.error('Zřejmě došlo k chybě při komunikaci se serverem, prosím opakujte akci později.', 'Data nebyla načtena', {
    //     positionClass: 'toast-bottom-right'
    //   });
    // })
  }

}
