import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

/* Service */
import {LoginService} from 'src/app/service/login/login.service';
import {AuthService}  from 'src/app/service/auth/auth.service';
import {NavService} from 'src/app/service/nav/nav.service';

/* Loader */
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm;

  constructor(private loginService: LoginService,
              private authService: AuthService,
              private router: Router,
              private spinnerService: Ng4LoadingSpinnerService) {}

  ngOnInit(){
    this.loginForm = new FormGroup({
      nickname: new FormControl('', [
        Validators.required
      ]),
      password: new FormControl('', [
        Validators.required
      ])
    });

    this.loginForm.badLoginTry = false;
  }

  getPassword() {
    return this.loginForm.get('password').value;
  }

  getNickname() {
    return this.loginForm.get("nickname").value;
  }

  login(){
    this.spinnerService.show();
    this.loginService.doLogin(this.getNickname(), this.getPassword()).then(response => {
        console.log("login response");
        this.spinnerService.hide();
        this.loginForm.badLoginTry = false;
        this.router.navigate(['../app/mainboard']);
    })
    .catch(error => {
        console.log("error in login component");
        this.spinnerService.hide();
        this.loginForm.badLoginTry = true;
        console.log(error);
    });

  }


}
