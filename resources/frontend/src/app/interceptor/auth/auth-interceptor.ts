import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

/*Commont http*/
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

/* Services */
import {AuthService} from 'src/app/service/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor{

  constructor(private authService: AuthService){}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.authService.getToken();

        console.log(token);
        console.log("fired auth");
        //token is valid and ready to use
        if (token) {
          const cloned = request.clone({
            headers: request.headers.set('Authorization', token)
          });

          console.log("Headers");
          console.log(cloned.headers);

          return next.handle(cloned);

        }
        else {
          return Observable.throw("No rights to access.");
        }

  }

}
