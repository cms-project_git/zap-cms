/* Angular modules */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* Components */
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { MainboardComponent } from './component/mainboard/mainboard.component';
import { SidebarComponent } from './component/partial/sidebar/sidebar.component';
import { ArticleOverviewComponent } from './component/article/article-overview/article-overview.component';
import { UserOverviewComponent } from './component/user/user-overview/user-overview.component';
import { ArticleListComponent } from './component/partial/article-list/article-list.component';

/* Services */
import { LoginService } from './service/login/login.service';
import { NavService } from './service/nav/nav.service';
import { ArticleService } from './service/article/article.service';

/* Commont http */
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

/* Modules */
import {RouteRoutingModule} from "./route-routing.module";

/* Environments */
import { environment } from 'src/environments/environment';

/* Clasess */
import { AuthInterceptor } from './interceptor/auth/auth-interceptor';

/* Loader */
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { ArticleAddComponent } from './component/article/article-add/article-add.component';

/* CK Editor */
import { CKEditorModule } from 'ngx-ckeditor';

/* Bootstrap */
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

/* Modals */
import { MediapoolModalComponent } from 'src/app/component/partial/modal/mediapool-modal/mediapool-modal.component';


/* Toaster */
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { UserListComponent } from './component/partial/user-list/user-list.component';
import { CategoryOverviewComponent } from './component/category/category-overview/category-overview.component';



@NgModule({
declarations: [
  AppComponent,
  LoginComponent,
  MainboardComponent,
  SidebarComponent,
  ArticleOverviewComponent,
  UserOverviewComponent,
  ArticleAddComponent,
  MediapoolModalComponent,
  ArticleListComponent,
  UserListComponent,
  CategoryOverviewComponent
],
imports: [
  BrowserModule,
  FormsModule,
  HttpClientModule,
  RouteRoutingModule,
  ReactiveFormsModule,
  Ng4LoadingSpinnerModule.forRoot(),
  CKEditorModule,
  NgbModule,
  BrowserAnimationsModule, // required animations module
  ToastrModule.forRoot() // ToastrModule added
  ],
  providers: [
    LoginService,
    NavService,
    ArticleService,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
