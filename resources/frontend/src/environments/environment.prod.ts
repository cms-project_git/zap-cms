export const environment = {
  production: true,
  apiEndpoint: "http://localhost:9080/api",
  apiSecureEndpoint: "http://localhost:9080/api/secure",
  endpoint: "http://localhost:9080"
};
