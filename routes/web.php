<?php
use Config as config;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route everything connected with app to index.html and check token */

Route::group(['prefix' => 'app', 'middleware' => ['jwt.verify.frontend']], function(){
  Route::get('/{any}', function ($any) {
    if (config('vars.debug_mode')){
      return redirect('http://localhost:4200');
    }
    else {
      View::addExtension('html', 'php');
      return View::make('index');
    }
  })->where('any', '.*');
});

//Login is accessible without token
Route::get('/login', function (){
    if (config('vars.debug_mode')){
      return redirect('http://localhost:4200/login');
    }
    else {
      View::addExtension('html', 'php');
      return View::make('index');
    }
})->name('login');
