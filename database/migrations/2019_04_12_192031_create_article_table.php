<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article', function (Blueprint $table) {
            $table->bigIncrements('id')
                ->autoIncrement();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->string('title', 80)
                ->unique()
                ->charset('utf8')
                ->collation('utf8_czech_ci');
            $table->string('perex', 500)
                ->charset('utf8')
                ->collation('utf8_czech_ci');
            $table->string('content', 5000)
                ->charset('utf8')
                ->collation('utf8_czech_ci');
            $table->string('slug', 90)
                ->unique()
                ->charset('utf8')
                ->collation('utf8_czech_ci');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('category_id');
            $table->string('cover', 100)
                ->charset('utf8')
                ->collation('utf8_czech_ci');

            $table->foreign('user_id')
                ->references('id')->on('user')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')->on('category')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
