<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->bigIncrements('id')
                ->autoIncrement();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->string('nickname', 25)
                ->unique()
                ->charset('utf8')
                ->collation('utf8_czech_ci');
            $table->string('name', 25)
                ->charset('utf8')
                ->collation('utf8_czech_ci');
            $table->string('surname', 30)
                ->charset('utf8')
                ->collation('utf8_czech_ci');
            $table->string('email', 70)
                ->unique()
                ->charset('utf8')
                ->collation('utf8_czech_ci');
            $table->string('password')
                ->charset('utf8')
                ->collation('utf8_czech_ci');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
